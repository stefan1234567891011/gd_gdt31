﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tower : MonoBehaviour
{
    public GameObject bullet;

    private bool enemyCanSpawn = true;
    void FixedUpdate()
    {
        if (Input.touchCount > 0)
        {
            Touch currentTouch = Input.touches[0];
            if (currentTouch.phase == TouchPhase.Began)
            {
                if (enemyCanSpawn)
                {
                    Instantiate(bullet);
                    enemyCanSpawn = false;
                }
            }
            if (currentTouch.phase == TouchPhase.Ended)
            {
                enemyCanSpawn = true;
            }
        }
    }
}
