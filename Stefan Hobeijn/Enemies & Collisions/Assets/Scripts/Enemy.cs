﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    public float speed;
    void Update()
    {
        gameObject.transform.position += Vector3.left * speed * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Bullet")
        {
            Destroy(other.gameObject);
            gameObject.transform.position = new Vector3(10, 0, 0);
        }
        else if (other.gameObject.tag == "Tower")
        {
            Destroy(other.gameObject);
        }
    }
}
