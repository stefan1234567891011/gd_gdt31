﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moveOnSwipe : MonoBehaviour
{
    public GameObject cube;
    public Rigidbody rbCube;

    private bool swiping = false;

    public const float swipeDistance = 20f;
    public const float cubeForce = 10f;

    private Vector2 touchPositionStart;
    private Vector2 touchPositionEnd;

    void FixedUpdate()
    {
        if (Input.touchCount > 0)
        {
            Touch currentTouch = Input.touches[0];

            if (currentTouch.phase == TouchPhase.Began)
            {
                touchPositionStart = currentTouch.position;
                touchPositionEnd = currentTouch.position;
                swiping = true;
            }
            if (currentTouch.phase == TouchPhase.Moved)
            {
                if (swiping)
                {
                    touchPositionEnd = currentTouch.position;
                    DetectSwipe();
                }
            }
            if (currentTouch.phase == TouchPhase.Ended)
            {
                swiping = false;
            }
        }
    }
    private void DetectSwipe()
    {
        float checkX = touchPositionStart.x - touchPositionEnd.x;
        float checkY = touchPositionStart.y - touchPositionEnd.y;
        if (checkX >= swipeDistance)
        {
            rbCube.AddForce(-cubeForce, 0, 0);
            swiping = false;
        }
        if (checkX <= -swipeDistance)
        {
            rbCube.AddForce(cubeForce, 0, 0);
            swiping = false;
        }
        if (checkY >= swipeDistance)
        {
            rbCube.AddForce(0, -cubeForce, 0);
            swiping = false;
        }
        if (checkY <= -swipeDistance)
        {
            rbCube.AddForce(0, cubeForce, 0);
            swiping = false;
        }
    }
}
